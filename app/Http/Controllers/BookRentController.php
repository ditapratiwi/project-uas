<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Book;
use App\Models\RentLogs;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\TryCatch;

class BookRentController extends Controller
{
    public function index()
    {
      $users = User::where('id', '!=', 2)->where('status', '!=', 'inactive')->get();
      $books = Book::all();
        return view('book-rent', ['users' => $users, 'books' => $books]);
    }

    public function store(Request $request)
    {
      $request['rent_date'] = Carbon::now()->toDateString();
      $request['return_date'] = Carbon::now()->addDay(3)->toDateString();

      $book = Book::findOrFail($request->book_id)->only('status');

      if($book['status'] != 'in stock') {
        Session::flash('message', 'Tidak bisa disewa, buku tidak tersedia');
        Session::flash('alert-class', 'alert-danger');
        return redirect('book-rent');
      }
      else{
         $count = RentLogs::where('user_id', $request->user_id)->where('actual_return_date', null)->count();
        
         if ($count >= 3)  {
            Session::flash('message', 'Tidak bisa disewa, pengguna telah mencapai batas buku');
            Session::flash('alert-class', 'alert-danger');
            return redirect('book-rent');
         }
         else{
            try {
                 DB::beginTransaction();
                //  proses memasukkan tabel sewa
                 RentLogs::create($request->all());
         
             //    proses update tabel buku
                $book = Book::findOrFail($request->book_id);
                $book->status = 'not available';
                $book->save();
         
                 DB::commit();
         
                 Session::flash('message', 'Sewa Buku Sukes');
                 Session::flash('alert-class', 'alert-success');
                 return redirect('book-rent');
         
                } catch (\Throwable $th) {
                 DB::rollBack();
                }     
         }
           
        }
    }
      public function returnBook()
      {
        $users = User::where('id', '!=', 2)->where('status', '!=', 'inactive')->get();
        $books = Book::all();

        return view('return-book', ['users' => $users, 'books' => $books]);
      }

      public function saveReturnBook(Request $request)
      {
        // pengguna & buku yg dipilih untuk di kembalikan benar, maka berhasil di kembalikan buku

        // pengguna & buku yg dipilih untuk dikembalikan salah, maka akan muncul notifikasi error
        $rent = RentLogs::where('user_id', $request->user_id)->where('book_id', $request->book_id)
        ->where('actual_return_date', null);
        $rentData = $rent->first();
        $countData = $rent->count();

        if($countData == 1) {
          // mengembalikan buku
          $rentData->actual_return_date = Carbon::now()->toDateString();
          $rentData->save();

          Session::flash('message', 'Buku Berhasil Dikembalikan');
            Session::flash('alert-class', 'alert-success');
            return redirect('book-return');
        }
        else{
          // notifikasi error muncul
          Session::flash('message', 'Ada Kesalahan Saat Proses');
            Session::flash('alert-class', 'alert-danger');
            return redirect('book-return');
        }
      }
}


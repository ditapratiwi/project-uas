@extends('layouts.mainlayout')

@section('title', 'Dashboard')

@section('content')

<h1>Selamat Datang, {{Auth::user()->username}}</h1>
   
<div class="row my-5">
    <div class="col-lg-4">
        <div class="card-data buku">
            <div class="row">
                <div class="col-6"><i class="bi bi-book"></i></div>
                <div class="col-6 d-flex flex-column justify-content-center align-item-end">
                    <div class="card-desc">Buku</div>
                    <div class="card-count ">{{$book_count}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card-data kategori">
            <div class="row">
                <div class="col-6"><i class="bi bi-card-list"></i></i></div>
                <div class="col-6 d-flex flex-column justify-content-center align-item-end">
                    <div class="card-desc">Kategori</div>
                    <div class="card-count ">{{$category_count}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card-data user">
            <div class="row">
                <div class="col-6"><i class="bi bi-people-fill"></i></div>
                <div class="col-6 d-flex flex-column justify-content-center align-item-end">
                    <div class="card-desc">Pengguna</div>
                    <div class="card-count ">{{$user_count}}</div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
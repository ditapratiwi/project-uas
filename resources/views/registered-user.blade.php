@extends('layouts.mainlayout')

@section('title', 'Registrasi Pengguna Baru')

@section('content')
    <h1>Daftar Pengguna Baru</h1>

    <div class="mt-5 d-flex justify-content-end">
        <a href="/users" class="btn btn-primary">Daftar Pengguna yang Disetujui</a>
     </div>

    <div class="my-5">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pengguna</th>
                    <th>No Telp</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($registeredUsers  as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->username }}</td>
                        <td>
                           @if ($item->phone)
                           {{ $item->phone}}
                        @else
                            -
                        @endif
                        </td>
                        <td>
                            <a href="/user-detail/{{$item->slug}}" class="btn btn-info">detail</a>
                        </td>
                    </tr>

                    @endforeach
            </tbody>
        </table>
    </div>
@endsection
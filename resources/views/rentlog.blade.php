@extends('layouts.mainlayout')

@section('title', 'Peminjaman Buku')

@section('content')
    <h1>Daftar Peminjaman  Buku</h1>

    <div class="mt-5">
        <x-rent-log-table  :rentlog='$rent_logs' />
    </div>
@endsection
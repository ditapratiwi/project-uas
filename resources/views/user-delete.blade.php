@extends('layouts.mainlayout')

@section('title', 'Hapus Pengguna')

@section('content')
   <h2>Apakah anda yakin untuk menghapus pengguna {{ $user->username}} ?</h2>
   <div class="mt-5">
        <a href="/user-destroy/{{$user->slug}}" class="btn btn-danger me-5">Yakin</a>
        <a href="/users" class="btn btn-info">Tidak</a>
   </div>
@endsection
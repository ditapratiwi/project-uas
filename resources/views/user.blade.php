@extends('layouts.mainlayout')

@section('title', 'Pengguna')

@section('content')
    <h1>Daftar User</h1>

    <div class="mt-5 d-flex justify-content-end">
        <a href="/user-banned" class="btn btn-secondary me-3">Tampilkan Pengguna yang Diblokir</a>
        <a href="/registered-users" class="btn btn-primary">Registrasi Pengguna Baru</a>
     </div>

     <div class="mt-5">
        @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
     </div>

    <div class="my-5">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pengguna</th>
                    <th>No Telp</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                    @foreach ($users as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->username }}</td>
                        <td>
                           @if ($item->phone)
                           {{ $item->phone}}
                        @else
                            -
                        @endif
                        </td>
                        <td>
                            <a href="/user-detail/{{$item->slug}}" class="btn btn-info">detail</a>
                            <a href="/user-ban/{{$item->slug}}" class="btn btn-warning">rincian larangan</a>
                        </td>
                    </tr>

                    @endforeach
            </tbody>
        </table>
    </div>
@endsection